'use strict';
const Controller = require('egg').Service;

class RepairmenController extends Controller {
  // 新增一个维修员
  async insertOneRepairmen() {
    const { ctx } = this;
    const resMsg = ctx.helper.resMsg();
    const payload = ctx.request.body;
    if (await ctx.service.repairmen.findRepairmenByAccount(payload.account)) {
      resMsg.msg = "用户名已存在！";
      resMsg.errcode = 1;
    } else {
      await ctx.service.repairmen.insertOneRepairmen(payload);
    }
    ctx.body = resMsg;
  }

  // 查询维修员
  async searchRepairmen() {
    const ctx = this.ctx;
    const resMsg = ctx.helper.resMsg();
    const conditions = ctx.request.body;
    const res = await ctx.service.repairmen.searchRepairmen(conditions);
    resMsg.data = res;
    ctx.body = resMsg;
  }

  // 修改维修员信息
  async updateRepairmen() {
    const ctx = this.ctx;
    const resMsg = ctx.helper.resMsg();
    const payload = ctx.request.body;
    const res = await ctx.service.repairmen.updateRepairmen(payload);
    if (res.errcode) {
      resMsg.errcode = res.errcode;
      resMsg.msg = res.msg;
    } else {
      resMsg.data = res.data;
    }
    ctx.body = resMsg;
  }

  // 删除维修员
  async delRepairmen() {
    const ctx = this.ctx;
    const resMsg = ctx.helper.resMsg();
    const payload = ctx.request.body;
    const res = await ctx.service.repairmen.delRepairmen(payload.id);
    if (res.errcode) {
      resMsg.errcode = res.errcode;
      resMsg.msg = res.msg;
    } else {
      resMsg.data = res.data;
    }
    ctx.body = resMsg;
  }
}

module.exports = RepairmenController;
