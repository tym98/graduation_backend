'use strict';
const Controller = require('egg').Service;

class AdminController extends Controller {
  // 新增一个管理员
  async insertOneAdmin() {
    const { ctx } = this;
    const resMsg = ctx.helper.resMsg();
    const payload = ctx.request.body;
    if (await ctx.service.admin.findAdminByAccount(payload.account)) {
      resMsg.msg = "用户名已存在！";
      resMsg.errcode = 1;
    } else {
      await ctx.service.admin.insertOneAdmin(payload);
    }
    ctx.body = resMsg;
  }

  // 查询管理员
  async searchAdmin() {
    const ctx = this.ctx;
    const resMsg = ctx.helper.resMsg();
    const conditions = ctx.request.body;
    const res = await ctx.service.admin.searchAdmin(conditions);
    resMsg.data = res;
    ctx.body = resMsg;
  }

    // // 修改维修员信息
    // async updateAdmin() {
    //   const ctx = this.ctx;
    //   const resMsg = ctx.helper.resMsg();
    //   const payload = ctx.request.body;
    //   const res = await ctx.service.admin.updateAdmin(payload);
    //   if (res.errcode) {
    //     resMsg.errcode = res.errcode;
    //     resMsg.msg = res.msg;
    //   } else {
    //     resMsg.data = res.data;
    //   }
    //   ctx.body = resMsg;
    // }
}

module.exports = AdminController;
