'use strict';
// 离校登记表
module.exports = app => {
    const mongoose = app.mongoose;
    const Schema = mongoose.Schema;

    const LeaveSchoolSchema = new Schema({
        // 学生id
        student_id: {
            type: Schema.Types.ObjectId,
            required: true,
            ref: 'Student',
        },
        // // 宿舍id
        // dorm_id: {
        //     type: Schema.Types.ObjectId,
        //     required: true,
        //     ref: 'Dorm',
        // },
        // 联系方式
        contact: {
            type: String,
            required: true,
        },
        // 离校去向
        destination: {
            type: String,
            required: true,
        },
        // 填写时间
        edit_date: {
            type: Number,
            required: true,
        },
        // 离校时间
        leave_date: {
            type: Number,
            required: true,
        },
        // 返校时间
        goback_date: {
            type: Number,
            required: true,
        },
        // 紧急联系人电话
        emergency_concact: {
            type: String,
            required: true,
        },
    });

    return mongoose.model('LeaveSchool', LeaveSchoolSchema);
};

