'use strict';
// 来访登记表
module.exports = app => {
    const mongoose = app.mongoose;
    const Schema = mongoose.Schema;

    const VisitorSchema = new Schema({
        // 来访人姓名
        visitor_name: {
            type: String,
            required: true,
        },
        // 来访人联系方式
        visitor_concact: {
            type: String,
            requried: true,
        },
        // 证件号
        visitor_id_num: {
            type: String,
            required: true,
        },
        // 事由
        reason: {
            type: String,
            required: true,
        },
        // 被访问人
        interviewer: {
            type: String,
        },
        // 来访时间
        visit_date: {
            type: Number, // 时间戳
            required: true,
        },
        // 离开时间
        leave_date: {
            type: Number,
        },
        // 来访问人数
        visitor_count: {
            type: Number,
        },
        // 来访状态
        visit_status: {
            type: Number,
            /**
             * 0 未离开
             * 1 已离开
             */
        },
    });

    return mongoose.model('Visitor', VisitorSchema);
};
