'use strict';
// 学生信息表
module.exports = app => {
  const mongoose = app.mongoose;
  const Schema = mongoose.Schema;

  const StudentSchema = new Schema({
    // 学号
    student_num: {
      type: String,
      unique: true,
      required: true,
    },
    // 密码
    student_password: {
      type: String,
      required: true,
    },
    // 学生姓名
    student_name: {
      type: String,
      required: true,
    },
    // 学生性别
    student_sex: {
      type: Number,
      required: true,
      /**
       * 1 男
       * 2 女
       */
    },
    // 寝室ID
    dorm_id: {
      type: Schema.Types.ObjectId,
      ref: 'Dorm'
    },
    // 地址
    student_address: {
      type: String,
    },
    // 班级
    student_class: {
      type: Number,
      required: true,
    },
    // 专业
    student_major: {
      type: String,
      required: true,
    },
    // 入学日期
    enroll_date: {
      type: String, // YYYY-MM-DD
    },
    // 居住状态
    reside_status: {
      type: Number,
      /**
       * -1 未入寝
       * 1 在住
       * 2 已退寝
       */
      required: true,
    },
    // 入住时间
    checkin_time: {
      type: Number,
    },
    // 学业状态
    study_status: {
      type: Number,
      /**
       * -1 未报到
       * 1 在读
       * 2 毕业
       */
      required: true,
    },
    // 身份
    identity: {
      type: Number,
      required: true,
      default: 2, // 学生的身份固定是2
    },

  });

  return mongoose.model('Student', StudentSchema);
};
