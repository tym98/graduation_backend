'use strict';
// 退寝表
module.exports = app => {
    const mongoose = app.mongoose;
    const Schema = mongoose.Schema;

    const CheckOutSchema = new Schema({
        // 学生id
        student_id: {
            type: Schema.Types.ObjectId,
            required: true,
            ref: 'Student',
        },
        // 退寝时间
        checkout_date: {
            type: Number, // 时间戳
            required: true,
        },
        // 寝室id
        dorm_id: {
            type: Schema.Types.ObjectId,
            required: true,
            ref: 'Dorm',
        },
    });

    return mongoose.model('CheckOut', CheckOutSchema);
};
