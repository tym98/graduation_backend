'use strict';
// 退寝申请表
module.exports = app => {
    const mongoose = app.mongoose;
    const Schema = mongoose.Schema;

    const LeaveDormSchema = new Schema({
        // 学生Id
        student_id: {
            type: Schema.Types.ObjectId,
            required: true,
            ref: 'Student',
        },
        // // 寝室号
        // dorm_num: {
        //     type: Number,
        //     required: true,
        // },
        // 联系方式
        contact: {
            type: String,
            required: true,
        },
        // 退寝理由
        reason: {
            type: String,
        },
        // 填写时间
        edit_date: {
            type: Number,
            required: true,
        },
        // // 退寝时间
        // leave_date: {
        //     type: Number,
        //     required: true,
        // },
        // // 紧急联系人电话
        // emergency_concact: {
        //     type: Number,
        //     required: true,
        // },
        // 审批时间
        approval_date: {
            type: Number,
        },
        // 审批人id
        approver: {
            type: Schema.Types.ObjectId,
            ref: 'Admin',
        },
        // 审批状态
        approval_status: {
            type: Number,
            default: -1,
            required: true,
            /**
             * -1 待审批
             * 1 已通过
             * 2 已驳回
             */
        },

    });

    return mongoose.model('LeaveDorm', LeaveDormSchema);
};
