'use strict';
// 寝室表
module.exports = app => {
    const mongoose = app.mongoose;
    const Schema = mongoose.Schema;

    const DormSchema = new Schema({
        // 寝室号
        dorm_num: {
            type: String,
            unique: true,
            required: true,
            /** 形如 8#604 */
        },
        // 所在楼栋ID
        building_id: {
            type: Schema.Types.ObjectId,
            ref: 'Building',
            required: true,
        },
        // 已住人数
        reside_count: {
            type: Number,
            required: true,
        },
        // 容纳人数
        contain_count: {
            type: Number,
            required: true,
        },
        // // 楼层号
        // floor_num: {
        //     type: Number,
        //     required: true,
        // },
        // 宿舍状态
        dorm_status: {
            type: Number,
            /**
             * -1 不可用
             * 1 可用
             */
            required: true,
        },
    });

    return mongoose.model('Dorm', DormSchema);
};
