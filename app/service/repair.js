'use strict';

const Service = require('egg').Service;

class RepairService extends Service {
  //  学生填写设备保修单
  async editRepairForm(data) {
    const { ctx } = this;
    const dorm = await ctx.model.Dorm.findOne({ dorm_num: data.dorm_num });
    if (!dorm) {
      return { errcode: 1, msg: "宿舍号不存在" };
    }
    console.log(dorm);

    data.initiator_id = data.id;
    data.dorm_id = dorm._id;
    data.repair_status = -1;
    data.repairmen_id = null;
    data.edit_date = Date.now();
    await ctx.model.Repair.create(data);
    return { errcode: 0 };
  }

  // 学生查看已提交的设备报修单
  async studentSubmittedRepairForm(id) {
    const { ctx } = this;
    const data = await ctx.model.Repair.find({ initiator_id: id })
      .populate('repairmen_id', 'repairmen_name -_id')
      .populate("dorm_id", "dorm_num")
      .sort('-edit_date');
    return { errcode: 0, data };

  }

  // 查询设备报修单
  async searchRepairForm(conditions) {
    const { ctx } = this;
    const page = conditions.page || 1;
    const pageSize = conditions.pageSize || 10;
    const keyword = new RegExp(conditions.keyword, 'i');
    const query = {
      repair_desc: { $regex: keyword }
    };
    if (conditions.status) {
      query.repair_status = conditions.status;
    }

    const [list, total] = await Promise.all([
      ctx.model.Repair.find(query)
        .sort('-edit_date')
        .populate('initiator_id', '-student_password')
        .populate('repairmen_id', 'repairmen_name')
        .populate("dorm_id", "dorm_num")
        .skip((page - 1) * pageSize)
        .limit(pageSize),
      ctx.model.Repair.find(query).count(),
    ]);

    return {
      list,
      paging: ctx.helper.getPaging(total, pageSize)
    };
  }

  // 开始处理维修单
  async startHandlerRepairForm(id, repairmen) {
    const { ctx } = this;
    const repair = await ctx.model.Repair.findById(id);
    if (!(repair.repair_status === -1 || repair.repair_status === 3)) {
      return { errcode: 1, msg: "维修单状态已更变" };
    }
    repair.repair_status = 1;
    repair.repairmen_id = repairmen;
    await repair.save();
    return { errcode: 0 };
  }

  // 将维修单设置为已完成
  async repairFormDone(id, repairmenId) {
    const { ctx } = this;
    const repair = await ctx.model.Repair.findById(id);
    if (!repairmenId.equals(repair.repairmen_id)) {
      return { errcode: 1, msg: "该报修单已由其他人接手" };
    }
    if (!(repair.repair_status === 1)) {
      return { errcode: 1, msg: "维修单状态已更变" };
    }
    repair.repair_status = 2;
    await repair.save();
    return { errcode: 0 };
  }

  // 将维修单设置为另约时间
  async makeRepairFormAnotherTime(id, repairmenId) {
    const { ctx } = this;
    const repair = await ctx.model.Repair.findById(id);
    if (!repairmenId.equals(repair.repairmen_id)) {
      return { errcode: 1, msg: "该报修单已由其他人接手" };
    }
    if (!(repair.repair_status === 1)) {
      return { errcode: 1, msg: "维修单状态已更变" };
    }
    repair.repair_status = 3;
    await repair.save();
    return { errcode: 0 };
  }

  // 删除维修单
  async delRepairForm(id) {
    const { ctx } = this;
    await ctx.model.Repair.deleteOne({ _id: id });
    return { errcode: 0 };
  }
}

module.exports = RepairService;
